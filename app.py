from __future__ import print_function

from flask import Flask, render_template, redirect, url_for, request, g
from flask import session, abort, flash, jsonify
from flask_sslify import SSLify
from flask_caching import Cache
from flask_mysqlpool import MySQLPool
import json
import os
import datetime
import pymysql
import requests
import socket
import os.path
import flask
import re
import urllib.request
import logging
import string 
import random
import regex as re






app = Flask(__name__)

app.config['TEMPLATES_AUTO_RELOAD'] = True

hst = "";
m_hst = "";
usr = "";
pwd = "";


# if(ip.startswith("94.237")):
#     hst = "10.2.10.122"
#     m_hst = "10.2.9.157"
#     usr = "root"
#     pwd = "Admin.902.14"
#     app.debug = False
#     config={'CACHE_TYPE': 'redis', 'CACHE_REDIS_URL': 'redis://10.2.2.183:6379/0'}
#     app.config['MYSQL_HOST'] = hst
#     app.config['MYSQL_PORT'] = 3306
#     app.config['MYSQL_USER'] = usr
#     app.config['MYSQL_PASS'] = pwd
#     app.config['MYSQL_DB'] = 'twitics'
#     app.config['MYSQL_POOL_NAME'] = 'mysql_pool'
#     app.config['MYSQL_POOL_SIZE'] = 32
#     app.config['MYSQL_AUTOCOMMIT'] = True
#     #sys.path.append('/root/miniconda2/lib/python2.7/site-packages') # Replace this with the place you installed facebookads using pip
#     #sys.path.append('/root/miniconda2/lib/python2.7/site-packages/facebook_business-3.0.0-py2.7.egg-info') # same as above
#     print("Running in production mode")


# else:
hst = "localhost"
usr = "root"
pwd = ""
app.debug = True
config={'CACHE_TYPE': 'redis', 'CACHE_REDIS_URL': 'redis://localhost:6379/3'}

app.secret_key = os.urandom(12)


def get_db():
     db = pymysql.connect(host='localhost', user='root',
                          passwd='', db="urhope_db", charset='utf8mb4')
     return db



# Route for Base template
@app.route('/')
def home():
    return render_template('index.html')

@app.route('/form')
def form():
    return render_template('form.html')
    
@app.route('/team-members')
def members():
    return render_template('team-members.html')


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)



